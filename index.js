const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const artists = require('./routers/artists');
const albums = require('./routers/albums');
const tracks = require('./routers/tracks');

const server = express();
server.use(cors());
server.use(express.json());

const port = 7000;

server.get('/', (req,res) => {
   res.send('Hello')
})

server.use('/artists', artists);
server.use('/albums', albums);
server.use('/tracks', tracks);

const run = async () => {
   await mongoose.connect('mongodb://localhost/musicStorage');

   server.listen(port, () => {
      console.log(`Server is started on ${port} port !`);
   })

   exitHook(() => {
      console.log('exiting');
      mongoose.disconnect();
   });
}
run().catch(e => console.error(e));