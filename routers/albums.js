const express = require('express');
const router = express.Router();
const Album = require('../models/Album');
const giveCheckedEssence = require('../utilities/giveCheckedEssence');

router.get('/', async (req, res) => {
   if (req.query.artist) {
      try {
         const artists = await Album.findOne({author: req.query.artist}).populate('author', 'name');
         res.send(artists);
      } catch (e) {
         res.sendStatus(500)
      }
   } else {
      try {
         const artists = await Album.find().populate('author', 'name');
         res.send(artists);
      } catch (e) {
         res.sendStatus(500)
      }
   }
});
router.get('/:id', async (req, res) => {
   try {
      const artists = await Album.findById(req.params.id).populate('author');
      res.send(artists);
   } catch (e) {
      res.sendStatus(404)
   }

});
router.post('/', async (req, res) => {
   const {title, author, image, date} = req.body;
   if (!title || !author) return res.sendStatus(400);
   const album = new Album(giveCheckedEssence({title, author, image, date}));
   try {
      await album.save();
      res.send(album);
   } catch (e) {
      res.sendStatus(400)
   }
});
module.exports = router;