const express = require('express');
const router = express.Router();
const Artist = require('../models/Artist');
const giveCheckedEssence = require('../utilities/giveCheckedEssence');

router.get('/', async (req, res) => {
   try {
      const artists = await Artist.find();
      res.send(artists);
   } catch (e) {
      res.sendStatus(500)
   }
});
router.post('/', async (req, res) => {
   const {name, description, photo} = req.body;
   if (!name) return res.sendStatus(400);
   const artist = new Artist(giveCheckedEssence({name, description, photo}));
   try {
      await artist.save();
      res.send(artist);
   } catch (e) {
      res.sendStatus(500)
   }
});
module.exports = router;