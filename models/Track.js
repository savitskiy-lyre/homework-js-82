const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const TrackSchema = new mongoose.Schema({
   title: {
      type: String,
      required: true,
   },
   album: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'albums',
   },
   duration: String,
})

TrackSchema.plugin(idValidator);
const Track = mongoose.model('tracks', TrackSchema);
module.exports = Track;